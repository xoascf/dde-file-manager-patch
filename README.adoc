:project-name: dde-file-manager-patch
:project-description: Patch for the Deepin desktop environment file manager
:icons: font
:toc: preamble
:!toc-title:

= {project-name}

== {project-description}
